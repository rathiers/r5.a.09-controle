# r5.a.09-controle

## 1. Déroulement du TP

La procédure de mise en place de ces services avec Docker s'est déroulée comme suit :

Création du réseau externe

J'ai initié la configuration en créant un réseau appelé "external" à l'aide de la commande suivante :

`docker network create external`

Ce réseau servira de base pour les autres services afin d'ajouter des "noms de domaine". Pour chaque service créé, j'ai inclus la section "labels" dans la configuration appropriée, spécifiant le nom de domaine que j'ai également ajouté à mon fichier hosts. Les ports ont été laissés ouverts pour assurer la connectivité sur d'autres machines lors de la récupération des conteneurs.

# Dossier Traefik

J'ai élaboré un fichier docker-compose.yml pour déployer le service Traefik. Le lancement s'est effectué avec la commande :

`docker-compose up -d`

# Dossier Loki

Pour le dossier Loki, j'ai créé le fichier docker-compose.yml pour déployer le service avec Loki et Grafana. La commande de lancement est la suivante :

`docker-compose up -d`

Grafana permettra de visualiser les journaux, accessible via le port 3000 ou le nom de domaine "grafana.local".

# Dossier Ghost

Concernant le dossier Ghost, j'ai mis en place le fichier docker-compose.yml pour créer le service Ghost. J'ai ajouté le réseau "external" pour connecter Traefik, puis lancé le service avec la commande :

`docker-compose up -d`

L'accès se fait via le port 8100 ou le nom de domaine "ghost.local".

# Dossier Keycloak

Le dossier Keycloak a été configuré avec un fichier docker-compose.yml pour créer le service Keycloak. J'ai ajouté le réseau "external" pour la liaison avec Traefik puis lancé le service avec la commande :

`docker-compose up -d`

L'accès se fait via le port 8110 ou le nom de domaine "key.local".

# Dossier Minio

La configuration du dossier Minio a impliqué la création du fichier docker-compose.yml pour déployer le service Minio. J'ai intégré le réseau "external" pour la connexion avec Traefik puis lancé le service avec la commande :

`docker-compose up -d`

L'accès se fait via le port 9001 ou le nom de domaine "minio.local".

# Dossier Gitea

Enfin, pour le dossier Gitea, j'ai créé le fichier docker-compose.yml pour déployer le service Gitea. J'ai ajouté le réseau "external" pour la liaison avec Traefik puis lancé le service avec la commande :

`docker-compose up -d`

L'accès se fait via le port 3110 ou le nom de domaine "gitea.local".

# Conclusion

Pour résumer, j'ai dirigé l'ensemble de mes services avec Traefik en utilisant un réseau nommé "external". Il est possible d'accéder à ces services en utilisant les noms de domaine spécifiés dans le fichier hosts de ma machine (par exemple, 127.0.0.1 gitea.local). Pour permettre l'accès à ces noms de domaine depuis d'autres machines, il est nécessaire d'ajouter ces entrées dans le fichier hosts de ces machines respectives.

ps : tous fonctionnaient en classe, j'espère que serra le cas de votre côté.

## 2. Pour lancer les containers

Le fichier init.sh permet de lancer tous les containers en même temps.
Il faut faire les commandes :

`chmod +x init.sh`
<br>
`./init.sh`

ps : Ayant réalisée le TP sur windows, le fichier init.sh n'a pas été testé tel quel, il est donc possible qu'il y est des modifications a faire.
