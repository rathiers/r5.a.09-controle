#!/bin/bash

# lancer les conteneurs
docker compose traefik/docker-compose.yml up
docker compose loki/docker-compose.yml up 

docker compose ghost/docker-compose.yml up 
docker compose minio/docker-compose.yml up 
docker compose gitea/docker-compose.yml up 
docker compose keycloak/docker-compose.yml up 
